# CI/CD Sample Project

## Overview

This project serves as a demonstration of various CI/CD jobs using GitLab CI. The CI/CD pipeline is configured with different jobs to showcase the integration of security testing tools, such as:
- GitLeaks
- OWASP ZAP
- SAST (Static Application Security Testing)

## CI/CD Jobs

- GitLeaks Job

The GitLeaks job in this project is responsible for detecting sensitive information in the source code repository. It utilizes the `zricethezav/gitleaks` Docker image to perform a scan for potential leaks.

- OWASP ZAP Job

The OWASP ZAP job focuses on security scanning of a specified web application using the Zed Attack Proxy. It runs the baseline scan to identify common security vulnerabilities.

- SAST (Static Application Security Testing) Job

The SAST job performs static application security testing by analyzing the source code for potential security vulnerabilities. It is configured with various security analyzers such as Brakeman, ESLint, SpotBugs, etc.